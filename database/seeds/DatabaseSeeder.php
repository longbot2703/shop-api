<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            ['name' => 'ip4', 'price' => '500000', 'category' => 'phone'],
            ['name' => 'ip5', 'price' => '2000000', 'category' => 'phone'],
            ['name' => 'ip6', 'price' => '4000000', 'category' => 'phone'],
            ['name' => 'ip7', 'price' => '7000000', 'category' => 'phone'],
            ['name' => 'ip8', 'price' => '9000000', 'category' => 'phone'],
            ['name' => 'ipX', 'price' => '10000000', 'category' => 'phone'],
            ['name' => 'ip11', 'price' => '20000000', 'category' => 'phone'],
            ['name' => 'ss9', 'price' => '8000000', 'category' => 'phone'],
            ['name' => 'ss10', 'price' => '20000000', 'category' => 'phone'],
            ['name' => 'dell', 'price' => '20000000', 'category' => 'laptop'],
            ['name' => 'msi', 'price' => '15000000', 'category' => 'laptop'],
            ['name' => 'hp', 'price' => '15000000', 'category' => 'laptop'],
            ['name' => 'lenovo', 'price' => '15000000', 'category' => 'laptop'],
            ['name' => 'mac', 'price' => '25000000', 'category' => 'laptop'],
            ['name' => 'vaio', 'price' => '5000000', 'category' => 'laptop'],
            ['name' => 'asus', 'price' => '15000000', 'category' => 'laptop'],
        ]);
    }
}
