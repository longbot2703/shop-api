<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Request\ProductRequest;
use Illuminate\Database\QueryException;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $product = Product::all();
        return response() -> json ([
            'status code' => '200',
            'message' => 'Danh muc san pham',
            'product' => $product,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $sproduct = Product::create($request -> all());
        return response() -> json([
            'status code' => '200',
            'message' => 'Them sp thanh cong',
            'product' => $sproduct,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $products = Product::findOrFail($id);
        return response() -> json([
            'status code' => '200',
            'message' => 'Yeu cau thanh cong',
            'product' => $products,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $products = Product::findOrFail($id);
        $products->update($request -> all());
        return response() -> json([
            'status code' => '200',
            'message' => 'Update sp thanh cong',
            'product' => $products,
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $products = Product::findOrFail($id);
        $products->delete();
        return response() -> json([
            'status code' => '200',
            'message' => 'Xoa sp thanh cong',
        ]);
    }
}
